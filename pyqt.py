import ctypes
import threading

from PIL import ImageGrab, Image
from PyQt5.QtCore import Qt, QEvent, QSize, QRect, QPoint, pyqtSignal
from PyQt5.QtGui import QPainter, QColor, QPixmap, QImage, QBrush, QCursor, QIcon
from PyQt5.QtWidgets import QWidget, QSystemTrayIcon, QAction, qApp, QMenu, QDialog, QLabel, \
    QPushButton

from base import Base
from notify import Notify


class ScreenArea(QWidget):
    screen = None
    oldPic = None
    leftTop = [0, 0]
    rightBottom = [0, 0]
    mouseInRect = False
    mouseInResize = False
    widthWindow = None
    heightWindow = None

    show_should_update = False
    show_sc = False
    signal_update = pyqtSignal(str, name='signal_update')

    def __init__(self, app, board):
        super().__init__()
        app.setWindowIcon(QIcon('ico.png'))
        self.app = app
        app.setQuitOnLastWindowClosed(False)
        self.board = board

        self.init_ui()
        self.tray_icon = QSystemTrayIcon(self)
        self.tray_icon.setIcon(QIcon('ico.png'))
        self.tray_icon.show()
        self.tray_icon.activated.connect(self.show_screen)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.SplashScreen)

        # меню для трея
        my_app_id = 'boarddetection'
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(my_app_id)
        about_action = QAction("О программе", self)
        quit_action = QAction("Выход", self)
        about_action.triggered.connect(self.about_programm)
        quit_action.triggered.connect(qApp.quit)

        # сворачивание в трей
        tray_menu = QMenu()
        tray_menu.addAction(about_action)
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

        self.base = Base()
        self.signal_update.connect(self.should_update, Qt.QueuedConnection)

        # проверка обновлений программы
        threading.Thread(target=self.base.have_update, daemon=True, args=(self.signal_update,)).start()

    def about_programm(self):
        """
        Вывод окна с информацией о программе
        """
        d = QDialog()
        app_icon = QIcon()
        app_icon.addFile('ico/ico_16.png', QSize(16, 16))
        app_icon.addFile('ico/ico_24.png', QSize(24, 24))
        app_icon.addFile('ico/ico_32.png', QSize(32, 32))
        app_icon.addFile('ico/ico_48.png', QSize(48, 48))
        app_icon.addFile('ico/ico_48.png', QSize(256, 256))
        d.setWindowIcon(app_icon)

        d.setFixedSize(450, 265)
        d.setWindowTitle("О программе")
        d.setWindowFlags(d.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        d.setWindowModality(Qt.ApplicationModal)

        about_image = QLabel(d)
        about_image.setPixmap(QPixmap("img/about.jpg"))

        settings = self.base.get_settings()
        lab = QLabel(d)
        lab.setStyleSheet("margin-left: 10px; margin-top: 200px; font-size:12px;")
        lab.setText("Версия продукта - " + str(settings["version"]) + " \nПоследнее обновление - " + str(
            settings["last_update"]))

        link = QLabel(d)
        link.setStyleSheet("margin-left: 10px; margin-top: 240px; font-size:12px;")
        link.setText("<a href =\"http://webcreator34.ru\">http://webcreator34.ru</a>")
        link.setTextFormat(Qt.RichText)
        link.setTextInteractionFlags(Qt.TextBrowserInteraction)
        link.setOpenExternalLinks(True)
        d.exec_()

    def show_screen(self, i_reason):
        """
        Переход в режим выделения области
        :param i_reason: тип события
        """
        # если клик левой кнопкой мыши
        if i_reason == 3:
            self.screen = ImageGrab.grab().convert('RGB')
            self.reset()
            self.show()
            self.show_sc = True
            self.setFocusPolicy(Qt.StrongFocus)

    def reset(self):
        self.oldPic = None
        self.leftTop = [0, 0]
        self.rightBottom = [0, 0]
        self.mouseInRect = False
        self.mouseInResize = False
        self.setCursor(QCursor(Qt.ArrowCursor))

    def cursor_in_resize(self, x, y):
        """
        Проверка нахождения курсора в обласи выделения
        :param x: x-координата
        :param y: y-координата
        :return: False/True
        """
        # левый верхний угол выделения
        if self.leftTop[0] - 5 < x < self.leftTop[0] + 5 and self.leftTop[1] - 5 < y < self.leftTop[1] + 5:
            self.mouseInResize = 1
            return True

        # правый нижний угол выделения
        if self.rightBottom[0] - 5 < x < self.rightBottom[0] + 5 and \
                self.rightBottom[1] - 5 < y < self.rightBottom[1] + 5:
            self.mouseInResize = 2
            return True

        # левый нижний угол выделения
        if self.leftTop[0] - 5 < x < self.leftTop[0] + 5 and self.rightBottom[1] - 5 < y < self.rightBottom[1] + 5:
            self.mouseInResize = 3
            return True

        # правый верхний угол выделения
        if self.rightBottom[0] - 5 < x < self.rightBottom[0] + 5 and self.leftTop[1] - 5 < y < self.leftTop[1] + 5:
            self.mouseInResize = 4
            return True

        self.mouseInResize = False
        return False

    def init_ui(self):
        """
        Установка окна для выделения
        """
        self.screen = ImageGrab.grab().convert('RGB')
        flags = self.windowFlags() | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint
        self.setWindowFlags(flags)
        prim_screen = self.app.desktop().primaryScreen()
        d_size = self.app.desktop().screenGeometry(prim_screen).size()
        self.widthWindow = d_size.width()
        self.heightWindow = d_size.height()
        self.setMinimumSize(QSize(self.widthWindow, self.heightWindow))

    def paintEvent(self, e=False):
        """
        Отрисовка точек для resize area
        """

        painter = QPainter(self)
        painter.begin(self)
        pixmap = QPixmap(self.pil_to_qpixmap(self.screen))
        painter.drawPixmap(self.rect(), pixmap)
        brush = QBrush(Qt.Dense4Pattern)
        brush.setColor(QColor(116, 69, 20))
        r1 = self.highlight_area()
        for val in r1:
            painter.fillRect(val, brush)

        painter.setPen(Qt.white)
        painter.setBrush(Qt.black)
        painter.drawEllipse(QPoint(self.leftTop[0], self.leftTop[1]), 4, 4)
        painter.drawEllipse(QPoint(self.rightBottom[0], self.rightBottom[1]), 4, 4)
        painter.drawEllipse(QPoint(self.leftTop[0], self.rightBottom[1]), 4, 4)
        painter.drawEllipse(QPoint(self.rightBottom[0], self.leftTop[1]), 4, 4)
        painter.end()

    def highlight_area(self):
        """
        Заполнение области вне выделения
        :return: координаты вершин для заполнения области вне выделения
        """
        r1 = []
        r1.append(QRect(0, 0, self.leftTop[0], self.heightWindow))
        r1.append(QRect(self.leftTop[0], 0, self.rightBottom[0] - self.leftTop[0], self.leftTop[1]))
        r1.append(QRect(self.leftTop[0], 0, self.rightBottom[0] - self.leftTop[0], self.leftTop[1]))
        r1.append(QRect(self.rightBottom[0], 0, self.widthWindow - self.rightBottom[0], self.heightWindow))
        r1.append(QRect(self.leftTop[0], self.rightBottom[1], self.rightBottom[0] - self.leftTop[0],
                        self.heightWindow - self.rightBottom[1]))
        return r1

    def mousePressEvent(self, event):
        if event.buttons() == Qt.RightButton:
            self.create_screen_area()
        pos = event.pos()
        x = pos.x()
        y = pos.y()
        self.oldPic = [x, y]

    def mouse_move(self, x, y):
        if self.oldPic is not None:
            if self.cursor_in_resize(x, y):
                if self.mouseInResize == 1 or self.mouseInResize == 2:
                    self.setCursor(QCursor(Qt.SizeFDiagCursor))
                else:
                    self.setCursor(QCursor(Qt.SizeBDiagCursor))
                self.mouseInRect = False
            elif self.leftTop[0] < x < self.rightBottom[0] and self.leftTop[1] < y < self.rightBottom[1]:
                self.setCursor(QCursor(Qt.SizeAllCursor))
                self.mouseInRect = True
            else:
                self.setCursor(QCursor(Qt.ArrowCursor))
                self.mouseInRect = False

    @staticmethod
    def size_area(left_top, right_bottom):
        return (right_bottom[0] - left_top[0]) * (right_bottom[1] - left_top[1])

    def resize_area(self, x, y):
        """
        Изменения области выделения
        :param x: x-координата перетаскиваемой вершины
        :param y: y-координата перетаскиваемой вершины
        """
        if self.mouseInResize:
            left_top = self.leftTop
            right_bottom = self.rightBottom
            difference_x = (self.oldPic[0] - x) * (-1)
            difference_y = (self.oldPic[1] - y) * (-1)
            if self.mouseInResize == 1:
                left_top[0] += difference_x
                left_top[1] += difference_y
            elif self.mouseInResize == 2:
                right_bottom[0] += difference_x
                right_bottom[1] += difference_y
            elif self.mouseInResize == 3:
                left_top[0] += difference_x
                right_bottom[1] += difference_y
            elif self.mouseInResize == 4:
                right_bottom[0] += difference_x
                left_top[1] += difference_y
            self.oldPic = [x, y]
            if not self.size_area(left_top, right_bottom) < 50 and left_top[0] < right_bottom[0] and \
                    left_top[1] < right_bottom[1]:
                self.leftTop = left_top
                self.rightBottom = right_bottom
                self.update()

    @staticmethod
    def pil_to_qpixmap(pil_img):
        """
        Преобразование PIL-изображения в QPixMap
        :param pil_img: PIL-изображение
        :return: QPixMap_изображение
        """
        if pil_img.mode == "RGB":
            r, g, b = pil_img.split()
            im = Image.merge("RGB", (b, g, r))
        elif pil_img.mode == "RGBA":
            r, g, b, a = pil_img.split()
            im = Image.merge("RGBA", (b, g, r, a))
        elif pil_img.mode == "L":
            im = pil_img.convert("RGBA")
        im2 = im.convert("RGBA")
        data = im2.tobytes("raw", "RGBA")
        qim = QImage(data, im.size[0], im.size[1], QImage.Format_ARGB32)
        pixmap = QPixmap.fromImage(qim)
        return pixmap

    def eventFilter(self, source, event):
        if event.type() == QEvent.MouseMove:
            pos = event.pos()
            x = pos.x()
            y = pos.y()
            if self.show_sc is True:
                if event.buttons() == Qt.NoButton:
                    self.mouse_move(x, y)
                elif self.mouseInRect:
                    self.drag_area(x, y)
                elif self.mouseInResize:
                    self.resize_area(x, y)
                else:
                    self.select_area(x, y)
        return QWidget.eventFilter(self, source, event)

    def drag_area(self, x, y):
        """
        Перетаскивание области выделения
        :param x: x-координата мыши при перетаскивании
        :param y: y-координата при перетаскивании
        """
        difference_x = (self.oldPic[0] - x) * (-1)
        difference_y = (self.oldPic[1] - y) * (-1)
        self.leftTop[0] += difference_x
        self.rightBottom[0] += difference_x
        self.leftTop[1] += difference_y
        self.rightBottom[1] += difference_y
        self.oldPic = [x, y]
        self.update()

    def select_area(self, x, y):
        if self.oldPic is None:
            self.oldPic = [x, y]
        else:
            self.leftTop = [min(self.oldPic[0], x), min(self.oldPic[1], y)]
            self.rightBottom = [max(self.oldPic[0], x), max(self.oldPic[1], y)]
            self.update()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.hide()
            self.reset()

    def create_screen_area(self):
        self.show_sc = True
        self.hide()
        img = self.screen.crop((self.leftTop[0], self.leftTop[1], self.rightBottom[0], self.rightBottom[1]))
        self.board.start_detect(img, self)

    def send_message(self, txt):
        """
        Создание модального окна
        :param txt: Сообщение
        """
        Notify(txt, self.app)

    def should_update(self, about):
        """
        Демонстрация окна для обновления баз
        :param about: Информация об обновлении
        """
        if self.show_should_update is True:  # если окно уже открыто
            return

        self.d = QDialog()
        self.d.setWindowFlag(Qt.WindowCloseButtonHint, False)
        self.d.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.d.setStyleSheet("background: white;")

        self.d.setFixedSize(480, 250)
        self.d.setWindowTitle("Обновление Board detection")
        self.d.setWindowFlags(self.d.windowFlags() & ~Qt.WindowContextHelpButtonHint)
        self.d.setWindowModality(Qt.ApplicationModal)

        about_image = QLabel(self.d)
        about_image.setPixmap(QPixmap("img/logo.png"))
        about_image.setStyleSheet("margin-left: 280px;")

        lab = QLabel(self.d)
        lab.setStyleSheet("margin-left: 10px; margin-top: 50px; font-size:14px; text-align:center;")
        lab.setText("Найдена новая версия базы данных для поиска.\nВ новой версии: \n" + about + "\n\nОбновить?")

        update = QPushButton("", self.d)
        update.setStyleSheet(""
                             "width:170px; height:46px; background: url('img/button_update.png'); border: 0px;")
        update.move(20, 180)
        update.clicked.connect(self.update_base)

        ignore = QPushButton("", self.d)
        ignore.setStyleSheet(
            "width:170px; height:46px; background: url('img/button_ignore.png'); border: 0px;")
        ignore.move(290, 180)
        ignore.clicked.connect(self.ignore_update)
        self.show_should_update = True
        self.d.exec_()

    def update_base(self):
        """
        Запуск процесса обновления базы
        """
        self.d.hide()
        self.base.update_json_model()
        self.base.update_h5_model()
        self.base.set_update()
        self.send_message("Базы успешно обновлены")
        self.show_should_update = False

    def ignore_update(self):
        """
        Установка игнорирования обновления
        """
        self.base.ignore_update()
        self.d.close()
        self.show_should_update = False
